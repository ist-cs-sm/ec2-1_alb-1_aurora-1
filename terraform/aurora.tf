resource "aws_rds_cluster" "default" {
  engine = "aurora-mysql"
  cluster_identifier              = "${var.projectname}"
  master_username                 = "${var.dbusername}"
  master_password                 = "${var.dbpassword}"
  backup_retention_period         = 5
  preferred_backup_window         = "19:30-20:00"
  preferred_maintenance_window    = "wed:20:15-wed:20:45"
  port                            = 3306
  vpc_security_group_ids          = ["${aws_security_group.Sg-localaccess.id}"]
  db_subnet_group_name            = "${aws_db_subnet_group.main.id}"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.default.name}"

  tags = {
    Name    = "${var.site_domain}"
  }
}

resource "aws_rds_cluster_instance" "default" {
  engine = "aurora-mysql"
  identifier              = "${var.projectname}-rw"
  cluster_identifier      = "${aws_rds_cluster.default.id}"
  instance_class          = "${var.cluster_instance_class}"
  db_subnet_group_name    = "${aws_db_subnet_group.main.id}"
  db_parameter_group_name = "${aws_db_parameter_group.default.name}"


  tags = {
    Name    = "${var.site_domain}"
  }
}