## Security Group (ALB) forIST

resource "aws_security_group" "Sg-alb" {
  name        = "for ALB"
  description = "Allow local traffic."
  vpc_id      = "${aws_vpc.vpc.id}"

    tags = {
    Name = "Sg-alb-${var.site_domain}"
  }

}

resource "aws_security_group_rule" "inbound-alb-80" {
  security_group_id = aws_security_group.Sg-alb.id
  type        = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound-alb-443" {
  security_group_id = aws_security_group.Sg-alb.id
  type        = "ingress"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "alb-outband" {
  security_group_id = aws_security_group.Sg-alb.id
  type        = "egress"
  from_port = 0
  to_port = 0
  protocol = -1
  cidr_blocks = ["0.0.0.0/0"]
}