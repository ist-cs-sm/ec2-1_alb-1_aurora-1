## Security Group (SSH) forIST

resource "aws_security_group" "Sg-localaccess" {
  name        = "localaccess"
  description = "Allow local traffic."
  vpc_id      = "${aws_vpc.vpc.id}"

    tags = {
    Name = "Sg-localaccess-${var.site_domain}"
  }

}

resource "aws_security_group_rule" "inbound-localaccess" {
  security_group_id = aws_security_group.Sg-localaccess.id
  type        = "ingress"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "outbound_localaccess" {
  security_group_id = aws_security_group.Sg-localaccess.id
  type        = "egress"
  from_port = 0
  to_port = 0
  protocol = -1
  cidr_blocks = ["0.0.0.0/0"]
}