## Private subnet

resource "aws_subnet" "private_subnet_1a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "10.0.10.0/24"
  availability_zone = "ap-northeast-1a"

  tags = {
    Name = "Subnet-private-${var.site_domain}-1a"
  }
}

resource "aws_subnet" "private_subnet_1c" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "10.0.20.0/24"
  availability_zone = "ap-northeast-1c"

  tags = {
    Name = "Subnet-private-${var.site_domain}-1c"
  }
}