
# ---------------
# variables.tf
# ---------------


variable "projectname" {
  default = "PROJECT"
}

variable "site_domain" {
  default = "CNAME"
}

variable "root_domain" {
  default = "ROOTDOMAIN"
}

variable "stage" {
  default = "STAGE"
}

variable "instancetype" {
  default = "INSTANCETYPE"
}

variable "EBSsize" {
  default = "EBSSIZE"
}

variable "dbusername" {
  default = "DBUSER"
}

variable "dbpassword" {
  default = "DBPASS"
}

variable "cluster_instance_class" {
  default = "DBINSTANCETYPE"
}

variable "key_name" {
  default = "PROJECT"
}


