## Security Group (ICMP) forIST

resource "aws_security_group" "Sg-IST-ICMP" {
  name        = "Sg-IST-ICMP"
  description = "Allow ICMP traffic."
  vpc_id      = "${aws_vpc.vpc.id}"

    tags = {
    Name = "Sg-IST-ICMP-${var.site_domain}"
  }

}

resource "aws_security_group_rule" "inbound_IST_ICMP" {
  security_group_id = aws_security_group.Sg-IST-ICMP.id
  type        = "ingress"
  from_port   = -1
  to_port     = -1
  protocol    = "icmp"
  cidr_blocks = [
    "124.35.96.10/32","124.35.96.13/32","202.248.28.196/32"
  ]
}

resource "aws_security_group_rule" "outbound_IST_ICMP" {
  security_group_id = aws_security_group.Sg-IST-ICMP.id
  type        = "egress"
  from_port = 0
  to_port = 0
  protocol = -1
  cidr_blocks = ["0.0.0.0/0"]
}

## Security Group (ICMP) for jigsaw

resource "aws_security_group" "Sg-JIGSAW-ICMP" {
  name        = "Sg-JIGSAW-ICMP"
  description = "Allow ICMP traffic."
  vpc_id      = "${aws_vpc.vpc.id}"

    tags = {
    Name = "Sg-JIGSAW-ICMP-${var.site_domain}"
  }

}

resource "aws_security_group_rule" "inbound_JIGSAW_ICMP" {
  security_group_id = aws_security_group.Sg-JIGSAW-ICMP.id
  type        = "ingress"
  from_port   = -1
  to_port     = -1
  protocol    = "icmp"
  cidr_blocks = [
    "124.38.245.90/32","221.245.250.58/32","203.141.136.194/32","210.229.179.170/32","211.128.86.128/27","58.12.243.208/28","221.245.235.248/29","35.200.36.54/32","35.200.63.242/32","35.197.17.40/32","35.199.191.1/32"
  ]
}

resource "aws_security_group_rule" "outbound_JIGSAW_ICMP" {
  security_group_id = aws_security_group.Sg-JIGSAW-ICMP.id
  type        = "egress"
  from_port = 0
  to_port = 0
  protocol = -1
  cidr_blocks = ["0.0.0.0/0"]
}