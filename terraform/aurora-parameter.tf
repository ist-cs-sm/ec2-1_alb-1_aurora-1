resource "aws_db_parameter_group" "default" {
  name   = "param-${var.stage}-${var.projectname}"
  family = "aurora-mysql5.7"

  tags = {
    Name    = "${var.site_domain}"
  }
}

resource "aws_rds_cluster_parameter_group" "default" {
  name        = "clusterparam-${var.stage}-${var.projectname}"
  family      = "aurora-mysql5.7"
  description = "Cluster parameter group for ${var.site_domain}"

  tags = {
    Name    = "${var.site_domain}"
  }

  parameter {
    name         = "character_set_client"
    value        = "utf8mb4"
    apply_method = "immediate"
  }

  parameter {
    name         = "character_set_connection"
    value        = "utf8mb4"
    apply_method = "immediate"
  }

  parameter {
    name         = "character_set_database"
    value        = "utf8mb4"
    apply_method = "immediate"
  }

  parameter {
    name         = "character_set_filesystem"
    value        = "utf8mb4"
    apply_method = "immediate"
  }

  parameter {
    name         = "character_set_results"
    value        = "utf8mb4"
    apply_method = "immediate"
  }

  parameter {
    name         = "character_set_server"
    value        = "utf8mb4"
    apply_method = "immediate"
  }

  parameter {
    name         = "collation_connection"
    value        = "utf8mb4_general_ci"
    apply_method = "immediate"
  }

  parameter {
    name         = "collation_server"
    value        = "utf8mb4_general_ci"
    apply_method = "immediate"
  }

  parameter {
    name         = "time_zone"
    value        = "Asia/Tokyo"
    apply_method = "immediate"
  }
}

resource "aws_db_subnet_group" "main" {
  name       = "subnet-${var.stage}-${var.projectname}"
  subnet_ids = [aws_subnet.private_subnet_1a.id,aws_subnet.private_subnet_1c.id]

  tags = {
    Name = " Aurora subnet group"
  }
}