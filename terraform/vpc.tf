## VPC
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "VPC-${var.site_domain}"
  }
}

## Internet gateway

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "InternetGW-${var.site_domain}"
  }
}