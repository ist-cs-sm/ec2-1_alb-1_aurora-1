resource "aws_eip" "web1" {
  instance = aws_instance.web1.id
  vpc      = true
}

resource "aws_key_pair" "key_pair" {
    key_name   = "${var.key_name}"
    public_key = "${tls_private_key.keygen.public_key_openssh}"
  }

resource "aws_instance" "web1" {
    ami = data.aws_ami.centos7_ami.id
    instance_type = "${var.instancetype}"
    key_name = aws_key_pair.key_pair.key_name


    vpc_security_group_ids = [
      "${aws_security_group.Sg-localaccess.id}",
      "${aws_security_group.Sg-IST-SSH.id}",
      "${aws_security_group.Sg-JIGSAW-SSH.id}",
      "${aws_security_group.Sg-IST-ICMP.id}",
      "${aws_security_group.Sg-JIGSAW-ICMP.id}"
    ]
    subnet_id = "${aws_subnet.public_subnet_1a.id}"
    associate_public_ip_address = "false"
    ebs_block_device {
      device_name    = "/dev/sda1"
      volume_type = "gp2"
      volume_size = "${var.EBSsize}"
      }
    user_data   = "${file("./ec2-userdata-web1.sh")}"
    tags  = {
        Name = "web1"
    }
}
